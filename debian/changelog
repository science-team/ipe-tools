ipe-tools (1:7.2.29.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Fix Maintainer name of Debian Science team (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Fix day-of-week for changelog entry 20080517-1.

 -- Steve M. Robbins <smr@debian.org>  Fri, 22 Nov 2024 09:14:00 -0600

ipe-tools (1:7.2.24.1-4) unstable; urgency=medium

  [ Jeremy Bícha ]
  * [e82fde1] Add patch to fix build with poppler 24.06 (Closes: #1074400)

 -- Steve M. Robbins <smr@debian.org>  Sun, 11 Aug 2024 10:24:35 -0500

ipe-tools (1:7.2.24.1-3.1) unstable; urgency=high

  * Non-maintainer upload
  * Release to unstable for the poppler transition

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 10 Jan 2023 16:39:28 -0500

ipe-tools (1:7.2.24.1-3) experimental; urgency=medium

  [ Nathan Pratta Teodosio ]
  * Add patch to fix build with poppler 22.09.  Closes: #1026040.

 -- Steve M. Robbins <smr@debian.org>  Thu, 29 Dec 2022 10:27:31 -0600

ipe-tools (1:7.2.24.1-2) unstable; urgency=medium

  [ Sam Q ]
  * [eb01120] Fix for correcting error searching new upstream

  [ Steve Robbins ]
  * [010297f] Update standards version.

 -- Steve M. Robbins <smr@debian.org>  Sun, 16 Oct 2022 19:05:46 -0500

ipe-tools (1:7.2.24.1-1.1) unstable; urgency=medium

  * Non-maintainer upload

  [ Nathan Pratta Teodosio ]
  * Add patch to fix build with poppler 22.03 (Closes: #1012845)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 21 Aug 2022 18:11:26 -0400

ipe-tools (1:7.2.24.1-1) unstable; urgency=medium

  [ Steve Robbins ]
  * [db3b6a3] New upstream version 7.2.24.1
  * New version should build with poppler 22.  Closes: #1004977.

 -- Steve M. Robbins <smr@debian.org>  Sat, 19 Feb 2022 12:56:57 -0600

ipe-tools (1:7.2.20-1) unstable; urgency=medium

  [ Steve Robbins ]
  * [555859a] New upstream version 7.2.20
    Builds with poppler 0.85.  Closes: #968724.

 -- Steve M. Robbins <smr@debian.org>  Sun, 23 Aug 2020 23:37:33 -0500

ipe-tools (1:7.2.7.2-4) unstable; urgency=medium

  [ Steve Robbins ]
  * Remove recommendation on python-pil, since it is Python 2.  Closes: #936740.

 -- Steve M. Robbins <smr@debian.org>  Sat, 22 Feb 2020 10:20:58 -0600

ipe-tools (1:7.2.7.2-3) unstable; urgency=medium

  [ Helmut Grohne ]
  * Apply Helmut Grohne's fix for FTCBFS: (Closes: #949304)
    + Let dh_auto_build pass cross tools to make.
    + cross.patch: Make pkg-config substitutable.

 -- Steve M. Robbins <smr@debian.org>  Sun, 19 Jan 2020 13:38:31 -0600

ipe-tools (1:7.2.7.2-2) unstable; urgency=medium

  [ Steve Robbins ]
  * Upgrade svgtoipe to python3 and build --with python3.  Closes: #936740.

 -- Steve M. Robbins <smr@debian.org>  Fri, 17 Jan 2020 14:39:38 -0600

ipe-tools (1:7.2.7.2-1) unstable; urgency=medium

  * New upstream version 7.2.7.2
  * Team upload

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 27 Nov 2018 16:42:23 +0100

ipe-tools (1:7.2.7.1-1) unstable; urgency=medium

  * Update VCS fields
  * Team upload
  * New upstream version 7.2.7.1 (Closes: #911503, Closes: #910868)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 22 Oct 2018 11:27:17 +0200

ipe-tools (1:7.2.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version (added epoch to enable real versioning)
  * Add Vcs fields
  * cme fix dpkg-control; cme fix dpkg-copyright
  * debhelper 10
  * Replace python-imaging by python-pil
    Closes: #866435
  * Enhance descriptions
  * DEP5
  * Standards-Version: 4.1.3
  * hardening=+all
  * Add watch file

 -- Andreas Tille <tille@debian.org>  Mon, 08 Jan 2018 18:42:33 +0100

ipe-tools (20150406-3) unstable; urgency=medium

  [ Alexander Bürger ]
  * figtoipe.preinst: Remove.
  * figtoipe.postinst: New.  Remove old diversion properly.  Closes: #789281.

  [ Steve M. Robbins ]
  * Prepare for upload.

 -- Steve M. Robbins <smr@debian.org>  Wed, 24 Jun 2015 17:37:14 -0500

ipe-tools (20150406-2) unstable; urgency=medium

  * figtoipe.preinst: New.  Remove old diversion.  Closes: #789281.

 -- Steve M. Robbins <smr@debian.org>  Mon, 22 Jun 2015 21:53:31 -0500

ipe-tools (20150406-1) unstable; urgency=low

  [ Alexander Bürger ]
  * merged pdftoipe, svgtoipe, figtoipe, and ipe5toxml into a common source
    package as they share a common source repository
  * new upstream versions of figtoipe and ipe5toxml

  [ Steve M. Robbins ]
  * Team upload.

 -- Steve M. Robbins <smr@debian.org>  Sun, 07 Jun 2015 17:10:49 -0500

pdftoipe (20130124-1) unstable; urgency=low

  * New upstream.
    - patches/poppler.patch: Remove.  Applied upstream.
    - Upstream fixed their issue #112 for building with newer poppler.
      Closes: #679892.

  * control: Use debhelper 9, standards version 3.9.4.

 -- Steve M. Robbins <smr@debian.org>  Sun, 05 May 2013 20:00:47 -0500

pdftoipe (20110916-3) unstable; urgency=low

  * control: Migrate the libpoppler-dev build dependency to
    libpoppler-private-dev.  Closes: #660102.  Add build dependency on
    pkg-config.  Closes: #661082.

 -- Steve M. Robbins <smr@debian.org>  Thu, 23 Feb 2012 21:21:11 -0600

pdftoipe (20110916-2) unstable; urgency=low

  * patches/poppler.patch: New.  Fix to build with upcoming poppler 0.18
    (thanks, Pino Toscano).  Closes: #651151.

 -- Steve M. Robbins <smr@debian.org>  Sun, 11 Dec 2011 09:16:23 -0600

pdftoipe (20110916-1) unstable; urgency=low

  * New upstream.  Upstream renamed parseargs.c to parseargs.cc, so remove
    the rename from debian/rules.

 -- Steve M. Robbins <smr@debian.org>  Sun, 11 Dec 2011 02:00:30 -0600

pdftoipe (20091014-3) unstable; urgency=low

  * rules: Rename parseargs.c --> parseargs.cpp so it builds with c++
    compiler.  Required for Poppler > 0.15.  Closes: #618829.

 -- Steve M. Robbins <smr@debian.org>  Mon, 30 May 2011 23:54:45 -0500

pdftoipe (20091014-2) unstable; urgency=low

  * copyright: Update to reflect upstream license change to GPL v2.  See
    https://sourceforge.net/apps/mantisbt/ipe7/view.php?id=22.
    Closes: #609610.

 -- Steve M. Robbins <smr@debian.org>  Sun, 16 Jan 2011 13:05:44 -0600

pdftoipe (20091014-1) unstable; urgency=low

  * New upstream.
    - This version is compatible with Ipe 7 only.

 -- Steve M. Robbins <smr@debian.org>  Wed, 02 Jun 2010 21:26:05 -0500

pdftoipe (20070509-1) unstable; urgency=low

  * New upstream.

  * debian/watch: Update to use luaforge.net for download.
    Closes: #449909.

 -- Steve M. Robbins <smr@debian.org>  Thu, 07 Feb 2008 01:39:59 -0600

pdftoipe (20051114-1) unstable; urgency=low

  * New upstream.  Closes: #381003.

  * debian/control: Bump Standards-Version to 3.7.2.2 (no changes
    required).  Use debhelper compat version 5.

  * debian/watch: New.

 -- Steve M. Robbins <smr@debian.org>  Mon, 12 Mar 2007 00:55:41 -0500

pdftoipe (20040630-3) unstable; urgency=low

  * Rebuild with GCC-4 transitioned version of qt.

 -- Steve M. Robbins <smr@debian.org>  Sun,  4 Sep 2005 12:03:21 -0400

pdftoipe (20040630-2) unstable; urgency=low

  * Rebuild on system NOT polluted by experimental GCC.

 -- Steve M. Robbins <smr@debian.org>  Sat,  9 Apr 2005 13:02:37 -0400

pdftoipe (20040630-1) unstable; urgency=low

  * Initial upload.  Closes: 272125, 279853.

 -- Steve M. Robbins <smr@debian.org>  Sun,  3 Apr 2005 19:32:42 -0400

svgtoipe (20100608-1) unstable; urgency=low

  * Initial upload.

 -- Steve M. Robbins <smr@debian.org>  Sun, 11 Dec 2011 04:04:43 -0600

figtoipe (20080517-1) unstable; urgency=low

  * Initial release of figtoipe as a separate debian package, as it
    disappeared from the ipe package recently (Closes: #481022).

 -- Alexander Bürger <acfb@users.sf.net>  Wed, 21 May 2008 00:30:04 +0200

ipe5toxml (20051114-1) unstable; urgency=low

  * Initial upload.

 -- Steve M. Robbins <smr@debian.org>  Sun, 11 Dec 2011 11:46:41 -0600
